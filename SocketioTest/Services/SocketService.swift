//
//  Socket.swift
//  SocketioTest
//
//  Created by Franceskynov on 11/13/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation
import SocketIO

class SocketService {
    
    static func connection() -> SocketManager {
        
        return  SocketManager(socketURL: URL(string: Urls.socketUrl)!, config: [.log(true), .compress])
        
    }
}
