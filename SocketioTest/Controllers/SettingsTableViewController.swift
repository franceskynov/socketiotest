//
//  SettingsTableViewController.swift
//  SocketioTest
//
//  Created by Franceskynov on 11/13/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit
import SocketIO

class SettingsTableViewController: UITableViewController {

    
    @IBOutlet weak var messageLabel: UITextField!
    var alert: UIAlertController?
    let manager = SocketService.connection()
    lazy var socket = manager.defaultSocket
    override func viewDidLoad() {
        super.viewDidLoad()
        socket.connect()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        socket.on("greetings") { data, ack in
            //guard let message = data[0] as? String else { return }
            //guard let message = data[0] as? String else { return }
            //print(message)
            
            guard let me = data[0] as? [String: AnyObject] else { return }
            let message = me["message"] as? String as Any
            print(message)
            //print(data[0] as! String)
            ack.with("Got your currentAmount", "dude")
            
            self.alert = UIAlertController(title: "Messages from the web", message: (message as! String), preferredStyle: .alert)
            self.alert!.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.dismissAlert()
            }))
            
            self.present(self.alert!, animated: true)
        }
    }
    
    func dismissAlert(){
        self.alert!.dismiss(animated: true, completion: nil)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
  
    
    @IBAction func sendMessage(_ sender: UIButton) {
        socket.emit("from-ios", ["message": messageLabel.text])
    }
    
    @IBAction func settingsOptA(_ sender: UISwitch) {
        if sender.isOn {
            socket.emit("msg-from-ios", ["status": "on"])
        } else {
            socket.emit("msg-from-ios", ["status": "off"])
        }
    }
    

}
