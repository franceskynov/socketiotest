//
//  Socket.swift
//  SocketioTest
//
//  Created by Franceskynov on 11/14/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation

class Packet {
    var obj: [String: AnyObject]?
    
    init(dictionary: [String: AnyObject], keyName: String, to type: T) {
        self.obj = dictionary[keyName] as? T
    }
}
