//
//  Message.swift
//  SocketioTest
//
//  Created by Franceskynov on 11/14/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation

class Message {
    
    var message: String?
    
    init(dictionary: [String: AnyObject]) {
        self.message = dictionary["message"] as? String
    }
}
