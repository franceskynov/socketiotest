const app = require('express');
const server = require('http');
const io = require('socket.io')(server);

const port = 8000;

app.get('/', (req, res, next) => {
    res.sendFile(__dirname, + '/index.html');
});

io.on('connection', (socket) => {
    socket.on('msg-from-ios', (data) => {
        console.log(data)
    })
})

server.listen(port)